import 'package:flutter/material.dart';
import 'package:resume/resume.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Resume',
      theme: ThemeData(
        primarySwatch: Colors.indigo,
      ),
      home: resume(title: 'RESUME'),
    );
  }
}
