import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:resume/main.dart';
import 'package:url_launcher/url_launcher.dart';

class resume extends StatefulWidget {
  resume({Key? key, required this.title}) : super(key: key);

  final String title;

  @override
  _resume createState() => _resume();
}

class _resume extends State<resume> {
  final _normalfont = const TextStyle(fontSize: 18, color: Colors.white);

  var _details = home;

  void _changeDetail(int ord) {
    setState(() {
      if (ord == 3) {
        _details = cert;
      } else if (ord == 1) {
        _details = home;
      } else if (ord == 2) {
        _details = educ;
      } else if (ord == 4) {
        _details = cont;
      }
    });
  }

  static void openUrl(String url) async {
    await launch(url);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
          decoration: BoxDecoration(color: Colors.black87),
          child: ListView(
            children: <Widget>[
              Container(
                margin: const EdgeInsets.all(20),
                child: Image.asset(
                  'assets/image/profile.png',
                  height: 130,
                  width: 130,
                ),
              ),
              Container(
                  margin: const EdgeInsets.all(0),
                  child: Center(
                    child: Text(
                      'Anurak Yutthanawa',
                      style: TextStyle(
                          color: Colors.white,
                          fontSize: 25,
                          fontWeight: FontWeight.bold),
                    ),
                  )),
              Container(
                  margin: const EdgeInsets.all(10),
                  child: Row(
                      // scrollDirection: Axis.horizontal,
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Flexible(
                            child: TextButton(
                          onPressed: () {
                            _changeDetail(1);
                          },
                          child: Text('Home',
                              style: TextStyle(
                                  color: Colors.white54, fontSize: 14)),
                        )),
                        Flexible(
                            child: TextButton(
                          onPressed: () {
                            _changeDetail(2);
                          },
                          child: Text('EDU.',
                              style: TextStyle(
                                  color: Colors.white54, fontSize: 14)),
                        )),
                        Flexible(
                            child: TextButton(
                          onPressed: () {
                            _changeDetail(3);
                          },
                          child: Text('CERT.',
                              style: TextStyle(
                                  color: Colors.white54, fontSize: 14)),
                        )),
                        Flexible(
                            child: TextButton(
                          onPressed: () {
                            _changeDetail(4);
                          },
                          child: Text('CONT.',
                              style: TextStyle(
                                  color: Colors.white54, fontSize: 14)),
                        )),
                        Flexible(
                            child: TextButton(
                          onPressed: () {
                            openUrl('https://gitlab.com/Anu-01-rb');
                          },
                          child: Text('GitLab',
                              style: TextStyle(
                                  color: Colors.white54, fontSize: 14)),
                        )),
                        Flexible(
                            child: TextButton(
                          onPressed: () {
                            openUrl('https://github.com/Anu-01-rb');
                          },
                          child: Text('GitHub',
                              style: TextStyle(
                                  color: Colors.white54, fontSize: 14)),
                        )),
                      ])),
              _details,
            ],
          )),
    );
  }

  static Container _img(String image) {
    return Container(
      constraints: BoxConstraints(
        maxHeight: 80,
        maxWidth: 80,
      ),
      child: Image.asset(image),
    );
  }

  static ListTile _list(String str) {
    return ListTile(
      title: Text(
        ('- ' + str),
        style: _font,
      ),
    );
  }

  static Container _text(String str) {
    return Container(
      margin: const EdgeInsets.fromLTRB(20, 0, 20, 0),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.start,
        children: [
          Text(
            str,
            style: TextStyle(
                height: 2.0,
                fontSize: 18,
                color: Colors.white,
                fontWeight: FontWeight.w100),
            softWrap: true,
          )
        ],
      ),
    );
  }

  static var _headFont = TextStyle(fontSize: 30, color: Colors.white);

  static var _font = TextStyle(
      height: 2.0,
      fontSize: 16,
      color: Colors.white,
      fontWeight: FontWeight.w100);

  static var educ = Container(
      margin: const EdgeInsets.all(20),
      child: Column(children: [
        Row(
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            Container(
              margin: const EdgeInsets.fromLTRB(15, 0, 0, 0),
              child: Text(
                'BASIC EDUCATION.',
                style: _headFont,
              ),
            ),
          ],
        ),
        ListTile(
          title: Text(
            'Primary school: Hadsanookradbamrung School',
            style: _font,
          ),
        ),
        ListTile(
          title: Text(
            'Middle school : Prachuabwittayalai School',
            style: _font,
          ),
        ),
        ListTile(
          title: Text(
            'High school : Piboonbumpen Demonstration School, Burapha University',
            style: _font,
          ),
        ),
        Row(
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            Container(
              margin: const EdgeInsets.fromLTRB(15, 20, 0, 0),
              child: Text(
                'HIGHER EDUCATION.',
                style: _headFont,
              ),
            ),
          ],
        ),
        ListTile(
          title: Text(
            'B.Sc. : Computer Science, Burapha University (studying)',
            style: _font,
          ),
        ),
        Container(
          margin: const EdgeInsets.fromLTRB(0, 20, 0, 0),
          child: ExpansionTile(
            title: Text(
              'INTEREST.',
              style: _headFont,
            ),
            subtitle: Text(
              'Study area',
              style: _font,
            ),
            children: [
              _list('Computational Neuroscience'),
              _list('Biomedical Informatics'),
              _list('Cognitive Science'),
              _list('Algorithms Theory'),
              _list('Computing Theory'),
              _list('Cyber Security'),
              _list('Machine Learning'),
              _list('Artificial Intelligence'),
              _list('Quantum Computing'),
              _list('Grid Computing'),
            ],
          ),
        ),
        Row(
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            Container(
              margin: const EdgeInsets.fromLTRB(15, 20, 0, 0),
              child: Text(
                'SKILL.',
                style: _headFont,
              ),
            ),
          ],
        ),
        Container(
          margin: const EdgeInsets.fromLTRB(20, 30, 20, 6),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: <Widget>[
              Flexible(fit: FlexFit.loose, child: _img('assets/skill/r.png')),
              Flexible(fit: FlexFit.loose, child: _img('assets/skill/c.png')),
              Flexible(fit: FlexFit.loose, child: _img('assets/skill/cpp.png')),
              Flexible(
                  fit: FlexFit.loose, child: _img('assets/skill/java.png')),
              Flexible(fit: FlexFit.loose, child: _img('assets/skill/asm.png')),
              Flexible(
                  fit: FlexFit.loose, child: _img('assets/skill/dart.png')),
              Flexible(
                  fit: FlexFit.loose, child: _img('assets/image/python.png'))
              // _img('skill/r.png'),
              // _img('skill/c.png'),
              // _img('skill/cpp.png'),
              // _img('skill/java.png'),
              // _img('skill/asm.png'),
              // _img('skill/dart.png')
            ],
          ),
        ),
        Container(
          margin: const EdgeInsets.fromLTRB(20, 6, 20, 10),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            // crossAxisAlignment: CrossAxisAlignment.,
            children: <Widget>[
              Flexible(
                  fit: FlexFit.loose, child: _img('assets/skill/linux.png')),
              Flexible(
                  fit: FlexFit.loose, child: _img('assets/skill/flutter.png')),
              Flexible(
                  fit: FlexFit.loose, child: _img('assets/skill/docker.png')),
              Flexible(
                  fit: FlexFit.loose, child: _img('assets/skill/apache.png')),
              Flexible(
                  fit: FlexFit.loose, child: _img('assets/skill/nginx.png')),
              Flexible(
                  fit: FlexFit.loose, child: _img('assets/skill/andstd.png')),
              Flexible(
                  fit: FlexFit.loose, child: _img('assets/skill/sqlite3.png')),
              Flexible(
                  fit: FlexFit.loose, child: _img('assets/skill/vscode.png')),
              // _img('skill/flutter.png'),
              // _img('skill/docker.png'),
              // _img('skill/apache.png'),
              // _img('skill/nginx.png'),
              // _img('skill/andstd.png'),
              // _img('skill/sqlite3.png'),
              // _img('skill/vscode.png')
            ],
          ),
        )
      ]));

  static var cont = Container(
    margin: const EdgeInsets.all(20),
    child: Column(
      children: [
        Row(
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            Container(
              margin: const EdgeInsets.fromLTRB(15, 0, 0, 0),
              child: Text(
                'CONTACT.',
                style: _headFont,
              ),
            ),
          ],
        ),
        _text('Email: anu.01rob@outlook.com'),
        _text('Email: 63160015@my.buu.ac.th'),
        _text('tel: +66926907247'),
        Row(
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            Container(
              margin: const EdgeInsets.fromLTRB(15, 20, 0, 0),
              child: Text(
                'ADDRESS.',
                style: _headFont,
              ),
            ),
          ],
        ),
        _text('House No.: 802'),
        _text('House Vill.: 9'),
        _text('Aonoi sub-Districs'),
        _text('Maung Prachuab Khiri Khab Districs'),
        _text('Prachuap Khiri Khan Province'),
        _text('Post No.: 77210')
      ],
    ),
  );

  static var edu = Container(
      margin: const EdgeInsets.all(20),
      child: Column(children: [
        Row(
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            Container(
              margin: const EdgeInsets.fromLTRB(15, 0, 0, 0),
              child: Text(
                'BASIC EDUCATION.',
                style: _headFont,
              ),
            ),
          ],
        ),
        ListTile(
          title: Text(
            'Primary school: Hadsanookradbamrung School',
            style: _font,
          ),
        ),
        ListTile(
          title: Text(
            'Middle school : Prachuabwittayalai School',
            style: _font,
          ),
        ),
        ListTile(
          title: Text(
            'High school : Piboonbumpen Demonstration School, Burapha University',
            style: _font,
          ),
        ),
        Row(
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            Container(
              margin: const EdgeInsets.fromLTRB(15, 20, 0, 0),
              child: Text(
                'HIGHER EDUCATION.',
                style: _headFont,
              ),
            ),
          ],
        ),
        ListTile(
          title: Text(
            'B.Sc. : Computer Science, Burapha University (studying)',
            style: _font,
          ),
        ),
        Row(
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            Container(
              margin: const EdgeInsets.fromLTRB(15, 20, 0, 0),
              child: Text(
                'SKILL.',
                style: _headFont,
              ),
            ),
          ],
        ),
        _list('R Programming'),
        _list('C/C++ Programming'),
        _list('Java Progamming'),
        _list('Dart Progamming'),
        _list('Kotlin Progamming'),
        _list('Assembly[x86,ARM] Programming'),
        _list('SQL/NoSQL'),
        _list('Flutter'),
        _list('Docker'),
        _list('Apache'),
        _list('NginX'),
        _list('Linux Systems'),
        _list('Windows Systems'),
        _list('Networks Devices'),
        _list('Mobile Devices'),
        Row(
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            Container(
              margin: const EdgeInsets.fromLTRB(15, 20, 0, 0),
              child: Text(
                'INTEREST.',
                style: _headFont,
              ),
            ),
          ],
        ),
        _list('Computational Neuroscience'),
        _list('Biomedical Informatics'),
        _list('Cognitive Science'),
        _list('Algorithms Theory'),
        _list('Computing Theory'),
        _list('Cyber Security'),
        _list('Machine Learning'),
        _list('Artificial Intelligence'),
        _list('Quantum Computing'),
        _list('Grid Computing')
      ]));

  static var cert = Container(
      margin: const EdgeInsets.all(20),
      child: Column(
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              Container(
                margin: const EdgeInsets.fromLTRB(15, 0, 0, 0),
                child: Text(
                  'Hard Skill.',
                  style: TextStyle(fontSize: 30, color: Colors.white),
                ),
              ),
              Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Container(
                    margin: const EdgeInsets.all(5),
                    child: IconButton(
                      icon: const Icon(
                        Icons.download,
                        size: 20,
                      ),
                      color: Colors.white54,
                      onPressed: () {
                        openUrl(
                            'https://1drv.ms/u/s!AoJIfj5SgyHFhSLUjN5c6tK2FQ0e?e=PUqMcU');
                      },
                    ),
                  )
                ],
              )
            ],
          ),
          ListTile(
            title: Text(
              'Python Programming for Scientific Calculation : NU',
              style: _font,
            ),
          ),
          ListTile(
            title: Text(
              'Fundamental Programming for Information Systems : SUT',
              style: _font,
            ),
          ),
          ListTile(
            title: Text(
              'Fundamental Programming in C : KMUTT',
              style: _font,
            ),
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              Container(
                margin: const EdgeInsets.fromLTRB(15, 20, 0, 0),
                child: Text(
                  'Soft Skill.',
                  style: TextStyle(fontSize: 30, color: Colors.white),
                ),
              ),
              Container(
                margin: const EdgeInsets.fromLTRB(0, 20, 0, 0),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.end,
                  children: [
                    Container(
                      margin: const EdgeInsets.all(5),
                      child: IconButton(
                        icon: const Icon(
                          Icons.download,
                          size: 20,
                        ),
                        color: Colors.white54,
                        onPressed: () {
                          openUrl(
                              'https://1drv.ms/u/s!AoJIfj5SgyHFhSNDHnYAiBEP9aYg?e=5Gbm7E');
                        },
                      ),
                    )
                  ],
                ),
              )
            ],
          ),
          ListTile(
            title: Text(
              'Fundamental of Neuroscience : MU',
              style: _font,
            ),
          ),
          ListTile(
            title: Text(
              'Listening and Speaking for Communication : MU',
              style: _font,
            ),
          ),
          ListTile(
            title: Text(
              'Psychology and Daily life : CMU',
              style: _font,
            ),
          ),
          ListTile(
            title: Text(
              'English for TOEIC Preparation : BUU',
              style: _font,
            ),
          ),
        ],
      ));

  static Container home = Container(
      margin: const EdgeInsets.all(20),
      child: Column(
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.start,
            children: <Widget>[
              Container(
                margin: const EdgeInsets.fromLTRB(20, 0, 0, 0),
                child: Text(
                  'Hello!',
                  style: TextStyle(fontSize: 30, color: Colors.white),
                ),
              ),
            ],
          ),
          Row(
            children: [
              Flexible(
                  // decoration: BoxDecoration(color: Colors.white),
                  // margin: const EdgeInsets.fromLTRB(20, 10, 20, 20),
                  child: Container(
                margin: const EdgeInsets.fromLTRB(20, 10, 20, 20),
                child: Text(
                  'I\'m Anurak Yutthanawa a computer science student of the Faculty of Informatics, Burapha University. I currently am a Teacher Assistant of Fundamental Programming subjects for freshmen CS students. I\'m learning many platforms of MOOC and Online Learning such as ThaiMOOC, Edx, and OCW\'s MIT. And I\'m head of 29th CS student Burapha University in the same time.',
                  style: TextStyle(
                      height: 2.0,
                      fontSize: 18,
                      color: Colors.white,
                      fontWeight: FontWeight.w100),
                  softWrap: true,
                ),
              )),
            ],
          ),
          Row(
            children: [
              Flexible(
                  child: Container(
                margin: const EdgeInsets.fromLTRB(20, 0, 0, 0),
                child: Text(
                  'Basic Information',
                  style: _headFont,
                ),
              ))
            ],
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              Flexible(
                  child: Container(
                      margin: const EdgeInsets.fromLTRB(0, 10, 20, 20),
                      child: Column(
                        children: [
                          _text('Name: Anurak Yutthanawa'),
                          _text('Gender: Male'),
                          _text('Age: 19'),
                          _text('Birth: January,23 2002'),
                          _text('Major: Computer Science'),
                          _text('Institute: Burapha University'),
                          _text('Nationality: Thai'),
                        ],
                      ))),
            ],
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              Container(
                margin: const EdgeInsets.fromLTRB(20, 0, 0, 0),
                child: Text(
                  'Biography',
                  style: TextStyle(fontSize: 30, color: Colors.white),
                ),
              ),
            ],
          ),
          Row(
            children: [
              Flexible(
                  // decoration: BoxDecoration(color: Colors.white),
                  // margin: const EdgeInsets.fromLTRB(20, 10, 20, 20),
                  child: Container(
                margin: const EdgeInsets.fromLTRB(20, 10, 20, 20),
                child: Text(
                  'I was born in Jan 2002 in a medium family in the west countryside city of Thailand, I start kindergarten in the small school near my house. And study middle in the provincial school with the 5th ranking of all about thousand entrance student. Then I change school again in senior high school, I\'d come to study in the demonstration school of Burapha University aim to know much more advanced academic knowledge and system with the college environments.',
                  style: TextStyle(
                      height: 2.0,
                      fontSize: 18,
                      color: Colors.white,
                      fontWeight: FontWeight.w100),
                  softWrap: true,
                ),
              )),
            ],
          ),
        ],
      ));
}
